import requests
import string
import random
from bs4 import BeautifulSoup
import logging

logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',datefmt='%a, %d %b %Y %H:%M:%S',filename='username.log',filemode='w')
r = requests.get('http://121.40.172.109/register.jsp')
soup = BeautifulSoup(r.text)
cookies = dict(JSESSIONID = r.cookies['JSESSIONID'],jrun_sid = r.cookies['jrun_sid'], jrun_uid = '0')
formHashTag = soup.find("input",attrs={"name":"formHash"})
formHash = formHashTag.attrs['value']
availableName = "abcdefghijklmnopqrstuvwxyz1234567890_"
username = string.join(random.sample(list(availableName), 9)).replace(" ","")
password = string.join(random.sample(list(availableName), 10)).replace(" ","")
email = username + "@163.com"
payload = {'formHash':formHash,'username':username,'password':password,'password2':password,'email': email,'invitecode':'','questionid': '0','answer':'' ,'gender': '0','bday': '0000-00-00','location': '','site': '','qq': '','msn': '','icq': '','yahoo': '','taobao': '','alipay': '','bio': '','styleid': '0','tpp': '0','ppp': '0','timeoffset':'9999','timeformat': '0','dateformat': '0','pmsound': '1','showemail': '1','newsletter': '1','signature':'' }
headers = {'Referer':'http://121.40.172.109/register.jsp'}
regReq = requests.post('http://121.40.172.109/register.jsp?regsubmit=yes',data=payload,headers=headers)
logging.info("username: %s, password: %s" % (payload['username'],payload['password']))
